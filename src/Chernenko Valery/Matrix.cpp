#include"Matrix.h"

#include <iostream>
#include <string>
using namespace std;

typedef double ELEM;

Matrix::Matrix()
{
	columns = lines = 0;
	matrix = nullptr;
}

Matrix::Matrix(int _columns, int _lines)
{
	columns = _columns;
	lines = _lines;
	matrix = new ELEM*[columns];
	for (int i = 0; i < columns; ++i)
	{
		matrix[i] = new ELEM[lines];
		for (int j = 0; j < lines; j++)
			matrix[i][j] = 0;
	}
}

Matrix::Matrix(Matrix& _matrix)
{
	columns = _matrix.columns;
	lines = _matrix.lines;
	matrix = new ELEM*[columns];
	for (int i = 0; i < columns; ++i)
	{
		matrix[i] = new ELEM[lines];
		for (int j = 0; j < lines; j++)
			matrix[i][j] = _matrix[i][j];
	}
}

Matrix::~Matrix()
{
	for (int i = 0; i < columns; ++i)
			delete[] matrix[i];
	delete[] matrix;
}



void Matrix::SetRandomInt(int max)
{
	for (int i = 0; i < columns; i++)
		for (int j = 0; j < lines; j++)
			matrix[i][j] = (Elem)(rand() % max);
}

Matrix Matrix::deleteColumnAndLine(int column, int line)
{
	Matrix tmp(columns - 1, lines - 1);
	int CurI = 0;
	int CurJ = 0;
	for (int i = 0; i < columns; i++)
	{
		if (i == column)
			continue;
		CurJ = 0;
		for (int j = 0; j < lines; j++)
		{
			if (j == line)
				continue;
			tmp[CurI][CurJ] = matrix[i][j];
			CurJ++;
		}
		CurI++;
	}
	return tmp;
}

Elem Matrix::getDeterminant()
{
	int result = 0;
	if (columns != lines)
	{
		cout << "������� �� ����������! "<< endl;
		return result;
	}
	if (columns == 1)
		return matrix[0][0];
	for (int j = 0; j < lines; j++)
	{
		if (j % 2 == 0)
			result += matrix[0][j] * this->deleteColumnAndLine(0, j).getDeterminant();
		else
			result -= matrix[0][j] * this->deleteColumnAndLine(0, j).getDeterminant();
	}
	return result;
}

Matrix Matrix::getTranspose()
{
	Matrix tmp(lines, columns);
	for (int i = 0; i < columns; i++)
		for (int j = 0; j < lines; j++)
			tmp[j][i] = matrix[i][j];
	return tmp;
}

Matrix Matrix::getMatrixWithComplement()
{
	if (columns != lines)
	{
		cout << "���������� ��������� ������� �������������� ����������!" << endl;
		Matrix tmp;
		return tmp;
	}
	Matrix tmp(columns, lines);
	for(int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
		{
			tmp[i][j] = this->deleteColumnAndLine(i, j).getDeterminant();
			if ((j + i) % 2 == 1)
				tmp[i][j] *= -1;
		}
	return tmp;
}

Matrix Matrix::getInverseMatrix()
{
	int det = this->getDeterminant();
	if (det == 0)
	{
		cout << "�������� ������� �� ����������!" << endl;
		Matrix tmp;
		return tmp;
	}
	Matrix tmp;
	tmp = this->getMatrixWithComplement();
	tmp = tmp.getTranspose();
	tmp /= det;
	return tmp;
}



ostream& operator<<(ostream& out, const Matrix& _matrix)
{
	out << "������� �������� " << _matrix.columns << " �� " << _matrix.lines << endl;
	for (int i = 0; i < _matrix.columns; ++i)
	{
		out.precision(5);
		for (int j = 0; j < _matrix.lines; ++j)
			out << _matrix[i][j] << ' ';
		out << endl;
	}
	out << endl;
	return out;
}



Elem* Matrix::operator[](int i)
{
	return matrix[i];
}

const Elem* Matrix::operator[](int i) const
{
	return matrix[i];
}

Elem& Matrix::operator()(int i, int j)
{
	return *this[i][j];
}

const Elem& Matrix::operator()(int i, int j) const
{
	return *this[i][j];
}



Matrix& Matrix::operator=(const Matrix& _matrix)
{
	if (*this == _matrix)
		return *this;
	for (int i = 0; i < columns; ++i)
		delete[] matrix[i];
	delete[] matrix;

	columns = _matrix.columns;
	lines = _matrix.lines;
	matrix = new ELEM*[columns];
	for (int i = 0; i < columns; ++i)
	{
		matrix[i] = new ELEM[lines];
		for (int j = 0; j < lines; j++)
			matrix[i][j] = _matrix[i][j];
	}
	return *this;
}



Matrix Matrix::operator+(const Matrix& _matrix)
{
	if (this->columns != _matrix.columns || this->lines != _matrix.lines)
	{
		cout << "������� ���������� �������!" << endl;
		Matrix tmp(0, 0);
		return tmp;
	}
	else
	{
		Matrix tmp(columns, lines);
		for(int i = 0; i < columns; ++i)
			for (int j = 0; j < lines; ++j)
				tmp[i][j] = this->matrix[i][j] + _matrix[i][j];
		return tmp;
	}
}

Matrix Matrix::operator+(const Elem elem)
{
	Matrix tmp(columns, lines);
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			tmp[i][j] = this->matrix[i][j] + elem;
	return tmp;
}

Matrix& Matrix::operator+=(const Matrix& _matrix)
{
	if (this->columns != _matrix.columns || this->lines != _matrix.lines)
	{
		cout << "������� ���������� �������!" << endl;
		return *this;
	}
	else
	{
		for (int i = 0; i < columns; ++i)
			for (int j = 0; j < lines; ++j)
				this->matrix[i][j] = this->matrix[i][j] + _matrix[i][j];
		return *this;
	}
}

Matrix& Matrix::operator+=(const Elem elem)
{
	Matrix tmp(columns, lines);
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			this->matrix[i][j] = this->matrix[i][j] + elem;
	return *this;
}



Matrix Matrix::operator-(const Matrix& _matrix)
{
	if (this->columns != _matrix.columns || this->lines != _matrix.lines)
	{
		cout << "������� ���������� �������!" << endl;
		Matrix tmp(0, 0);
		return tmp;
	}
	else
	{
		Matrix tmp(columns, lines);
		for (int i = 0; i < columns; ++i)
			for (int j = 0; j < lines; ++j)
				tmp[i][j] = this->matrix[i][j] - _matrix[i][j];
		return tmp;
	}
}

Matrix Matrix::operator-(const Elem elem)
{
	Matrix tmp(columns, lines);
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			tmp[i][j] = this->matrix[i][j] - elem;
	return tmp;
}

Matrix& Matrix::operator-=(const Matrix& _matrix)
{
	if (this->columns != _matrix.columns || this->lines != _matrix.lines)
	{
		cout << "������� ���������� �������!" << endl;
		return *this;
	}
	else
	{
		for (int i = 0; i < columns; ++i)
			for (int j = 0; j < lines; ++j)
				this->matrix[i][j] = this->matrix[i][j] - _matrix[i][j];
		return *this;
	}
}

Matrix& Matrix::operator-=(const Elem elem)
{
	Matrix tmp(columns, lines);
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			this->matrix[i][j] = this->matrix[i][j] - elem;
	return *this;
}



Matrix Matrix::operator*(const Matrix& _matrix)
{
	if (this->lines != _matrix.columns)
	{
		cout << "������� ���������� �����������!" << endl;
		Matrix tmp(0, 0);
		return tmp;
	}
	else
	{
		Matrix tmp(columns, _matrix.lines);
		for(int i = 0; i < columns; i++)
			for (int j = 0; j < _matrix.lines; j++)
			{
				Elem summ = 0;
				for (int k = 0; k < lines; k++)
					summ += matrix[i][k] * _matrix[k][j];
				tmp[i][j] = summ;
			}
		return tmp;
	}
}

Matrix Matrix::operator*(const Elem elem)
{
	Matrix tmp(columns, lines);
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			tmp[i][j] = this->matrix[i][j] * elem;
	return tmp;
}

Matrix& Matrix::operator*=(const Matrix& _matrix)
{
	if (this->lines != _matrix.columns)
	{
		cout << "������� ���������� �����������!" << endl;
		return *this;
	}
	else
	{
		Matrix tmp(columns, _matrix.lines);
		for (int i = 0; i < columns; i++)
			for (int j = 0; j < _matrix.lines; j++)
			{
				Elem summ = 0;
				for (int k = 0; k < lines; k++)
					summ += matrix[i][k] * _matrix[k][j];
				tmp[i][j] = summ;
			}
		*this = tmp;
		return *this;
	}
}

Matrix& Matrix::operator*=(const Elem elem)
{
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			matrix[i][j] = this->matrix[i][j] * elem;
	return *this;
}



Matrix Matrix::operator/(const Elem elem)
{
	Matrix tmp(columns, lines);
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			tmp[i][j] = matrix[i][j] / elem;
	return tmp;
}

Matrix& Matrix::operator/=(const Elem elem)
{
	for (int i = 0; i < columns; ++i)
		for (int j = 0; j < lines; ++j)
			matrix[i][j] = matrix[i][j] / elem;
	return *this;
}



bool Matrix::operator==(const Matrix& _matrix)
{
	if (columns != _matrix.columns || lines != _matrix.lines)
		return false;
	for (int i = 0; i < columns; i++)
		for (int j = 0; j < lines; j++)
			if (matrix[i][j] != _matrix[i][j])
				return false;
	return true;
}

bool Matrix::operator!=(const Matrix& _matrix)
{
	return !(*this == _matrix);
}