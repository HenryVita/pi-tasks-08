#pragma once

#include <iostream>
#include <string>
using namespace std;

typedef double Elem;

class Matrix
{

private:

	int columns, lines;
	Elem** matrix;

public:

	Matrix();

	Matrix(int _columns, int _lines);

	Matrix(Matrix& _matrix);

	~Matrix();

	void SetRandomInt(int max);
	Matrix deleteColumnAndLine(int column, int line);
	Elem getDeterminant();
	Matrix getTranspose();
	Matrix getMatrixWithComplement();
	Matrix getInverseMatrix();

	friend ostream& operator<<(ostream& out, const Matrix& _matrix);

	Elem* operator[](int i);
	const Elem* operator[](int i) const;
	Elem& operator()(int i, int j);
	const Elem& operator()(int i, int j) const;

	Matrix& operator=(const Matrix& _matrix);

	Matrix operator+(const Matrix& _matrix);
	Matrix operator+(const Elem elem);
	Matrix& operator+=(const Matrix& _matrix);
	Matrix& operator+=(const Elem elem);

	Matrix operator-(const Matrix& _matrix);
	Matrix operator-(const Elem elem);
	Matrix& operator-=(const Matrix& _matrix);
	Matrix& operator-=(const Elem elem);

	Matrix operator*(const Matrix& _matrix);
	Matrix operator*(const Elem elem);
	Matrix& operator*=(const Matrix& _matrix);
	Matrix& operator*=(const Elem elem);

	Matrix operator/(const Elem elem);
	Matrix& operator/=(const Elem elem);

	bool operator==(const Matrix& _matrix);
	bool operator!=(const Matrix& _matrix);
};